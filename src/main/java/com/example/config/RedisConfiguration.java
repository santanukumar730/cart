package com.example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import com.example.model.CartLine;

@Configuration
public class RedisConfiguration {

	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
		jedisConFactory.setHostName("localhost");
//	    jedisConFactory.setPort(6379);
		return jedisConFactory;
	}

	/**
	 * 
	 * RP/DOHQR0700/DOHQR0700 SP/SU 24MAR21/0927Z R9X4UO <br/>
	 *
	 * SAN/KUM MR<br/>
	 * 2. QR 669 Q 01APR 4 CMBDOH HK1 0345 0600 *1A/E*<br/>
	 * 3. QR 663 Y 02APR 5 CMBDOH HK1 0415 0655 *1A/E*<br/>
	 * 4. QR 067 Y 02APR 5 DOHFRA HK1 0810 1340 *1A/E*<br/>
	 * 5. QR 665 Y 02APR 5 CMBDOH HK1 1050 1330 *1A/E*<br/>
	 * 6. QR 015 Y 02APR 5 DOHLHR HK1 1510 2025 *1A/E*<br/>
	 * 7. QR2655 Y 03APR 6 LHRJFK HK1       3  0945 1300   *1A/E*
	 * 8. AP H7875666441 <br>
	 * 9. TK OK24MAR/DOHQR0700//ETQR<br>
	 * 10. SSR OTHS 1A 872817491783 - FARE RULE OVERRIDES TKT DEADLINE IF MORE
	 * RESTRICTIVE <br>
	 * 11. SSR OTHS 1A 907429920576 - FARE RULE OVERRIDES TKT DEADLINE IF MORE
	 * RESTRICTIVE <br>
	 * 12. RM NOTIFY PASSENGER PRIOR TO TICKET PURCHASE & CHECK-IN: FEDERAL LAWS
	 * FORBID THE CARRIAGE OF HAZARDOUS MATERIALS - GGAMAUSHAZ/S7 <br>
	 * 13. FA PAX 157-2393749259/ETQR/QAR1220.00/24MAR21/DOHQR0700/6586 9042/S2 <br>
	 * 
	 * <p>
	 * Conflict of type Individual segment dupe is detected between <b>S2, S3 &
	 * S5</b>.<br/>
	 * 
	 * <ol>
	 * DSM checks decision priority:
	 * <li>"Ticketed vs un-ticketed": SM decides to mark <b>S3 & S5</b> for action
	 * as <b>S2</b> is Ticked.
	 * </ol>
	 * </br>
	 * Since <b>S3 & S5</b> is part of a married OnD and the OnD is not part of a
	 * conflict then the entire OnD is marked for action resulting in <b>[S3, S4] &
	 * [S5, S6]</b> being marked for action. <br/>
	 * <b>Final itinerary after action is<br/>
	 * 
	 * 2. QR 669 Q 01APR 4 CMBDOH HK1 0345 0600 *1A/E*<br/>
	 * 7. QR2655 Y 03APR 6 LHRJFK HK1 3 0945 1300 *1A/E* <br>
	 * </b>
	 * </p>
	 * 
	 * @throws Exception
	 */
	@Bean
	public RedisTemplate<String, CartLine> redisTemplate() {
		RedisTemplate<String, CartLine> template = new RedisTemplate<>();
		template.setConnectionFactory(jedisConnectionFactory());
		return template;
	}

}
